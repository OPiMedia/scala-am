.. -*- restructuredtext -*-

========
Scala-AM
========
**(Abstract) Abstract Machine Experiments using Scala**

**Fork** of Scala-AM (written by Quentin Stiévenart)
https://github.com/acieroid/scala-am
with the final **goal to parallelize** it (see Scala-Par-AM_).

* `scala-am/`_

  * `README.md`_
  * `src/main/scala/`_: **main sources** of this repository
  * `Scheme-examples/`_: **Scheme program examples** used to benchmarks Scala-Par-AM
  * `test/`_: Scheme program examples from the original Scala-AM

* `scala-am-master-20170914/`_:
  copy of the original Scala-AM (September 14, 2017) from GitHub
  https://github.com/acieroid/scala-am/commit/f00e530ff0e597f6eb613a93c055c80fcf23fe94

.. _`scala-am/`: https://bitbucket.org/OPiMedia/scala-am/src/master/scala-am/
.. _`README.md`: https://bitbucket.org/OPiMedia/scala-am/src/master/scala-am/README.md
.. _`scala-am-master-20170914/`: https://bitbucket.org/OPiMedia/scala-am/src/master/scala-am-master-20170914/
.. _Scala-Par-AM: https://bitbucket.org/OPiMedia/scala-par-am
.. _`Scheme-examples/`: https://bitbucket.org/OPiMedia/scala-am/src/master/scala-am/Scheme-examples/
.. _`src/main/scala/`: https://bitbucket.org/OPiMedia/scala-am/src/master/scala-am/src/main/scala/
.. _`test/`: https://bitbucket.org/OPiMedia/scala-am/src/master/scala-am/test/



Links
=====
* `scala-am.jar`_: the complete executable
* `HTML online documentation`_ from the source code
* Scala-Par-AM_: a parallel version of Scala-AM

.. _`HTML online documentation`: http://www.opimedia.be/DS/online-documentations/Scala-AM/html/
.. _`scala-am.jar`: https://bitbucket.org/OPiMedia/scala-am/downloads/scala-am.jar

|



Forked by  🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
====================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png



|Scala-AM|

`References for this picture`_

.. _`References for this picture`: https://bitbucket.org/OPiMedia/scala-am/src/master/_img/_src/_original/README.rst

.. |Scala-AM| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/18/2939354100-0-scala-am-logo_avatar.png
