import java.nio.file.{Path, Paths}
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.Duration

/**
  * Execute some abstract machines on some Scheme programs
  * depending of some parameters.
  *
  * Run with --help command line parameter to show all options:
  * $ java -cp scala-am.jar MainAsScalaParAM --help
  *
  * This main program is used to interact with Scala-Par-AM
  * [[https://bitbucket.org/OPiMedia/scala-par-am/]]
  * a parallel version of this Scala-AM.
  */
object MainAsScalaParAM {
  val scalaAMVersion: String = "Fork --- July 31, 2019"

  case class ParamsConfig(
    // Valid patterns for filenamesPattern:
    // https://docs.oracle.com/javase/8/docs/api/java/nio/file/FileSystem.html#getPathMatcher-java.lang.String-

    root: String = "",
    exceptsFilename: String = "",

    lattices: Seq[Config.Lattice.Value] = List(Config.Lattice.TypeSet),
    bounds: Seq[Int] = List(100),
    addresses: Seq[Config.Address.Value] = List(Config.Address.Classical),
    filenamesPattern: String = "regex:.+\\.scm",
    machines: Seq[Config.Machine.Value] = List(Config.Machine.AAMNS),

    filenamePrepare: String = "",
    nbRepetition: Int = 1,
    timeout: Option[Duration] = Some(Duration(1, "day")),

    headerEnabled: Boolean = true,
    printOldAAM: Boolean = false
  ) {
    override
    def toString: String = s"ParamsConfig:\troot: $root\texceptsFilename: $exceptsFilename\tlattices: $lattices\tbounds: $bounds\taddresses: $addresses\tfilenamesPattern: $filenamesPattern\tmachines: $machines\tfilenamePrepare: $filenamePrepare\tnbRepetition: $nbRepetition\ttimeout: $timeout\theaderEnabled: $headerEnabled"
  }

  val default: ParamsConfig = ParamsConfig()


  /**
    * For each file described in config,
    * for each machine, lattice from config.machines and config.lattices,
    * apply run(lattice, bound,
    *           address,
    *           file,
    *           machine,
    *           config.timeout, config.nbRepetition,
    *           config.printOldAAM).
    */
  def foreachFile[U](config: ParamsConfig) = {
    import java.nio.file.{Files, FileSystems, FileVisitResult, PathMatcher, SimpleFileVisitor}

    val excepts: Set[String] = readExcepts(config.exceptsFilename)
    val pairFiles:ArrayBuffer[(Path, List[String])] = ArrayBuffer.empty[(Path, List[String])]

    try {
      val pathMatcher: PathMatcher =
        FileSystems.getDefault().getPathMatcher(config.filenamesPattern)

      // Fill pairFiles array
      // with all files corresponding to filenamesPattern and not contain in exceptsFilename
      Files.walkFileTree(Paths.get(config.root),
        new SimpleFileVisitor[Path] {
          override
          def visitFile(path: Path, attrs: java.nio.file.attribute.BasicFileAttributes) = {
            if (pathMatcher.matches(path) && !excepts.contains(path.toString)) {
              pairFiles += ((path, path.toString.split('/').toList))
            }
            FileVisitResult.CONTINUE
          }
        })
    }
    catch {
        case _ : Throwable =>
    }

    // Comparison function used to sort filenames
    def lt(a: List[String], b: List[String]): Boolean = {
      def compare(a: String, b: String): Int = {
        val ucomp = a.toUpperCase.compareTo(b.toUpperCase)

        if (ucomp < 0) -1
        else if (ucomp > 0) 1
        else a.compareTo(b)
      }

      (a, b) match {
        case (a1::Nil, b1::Nil) => compare(a1, b1) < 0
        case (a1::Nil, b1::bs) => true
        case (a1::as, b1::Nil) => false
        case (a1::as, b1::bs) => {
          val c = compare(a1, b1)
          if (c != 0) c < 0 else lt(as, bs)
        }
        case _ => {
          assert(false)
          false
        }
      }
    }


    // Run machine on each lattice, Scheme program (in order), etc.
    for (lattice <- config.lattices;
         bound <- config.bounds;
         address <- config.addresses;
         file <- pairFiles.sortWith((a, b) => lt(a._2, b._2));
         machine <- config.machines) {
      run(lattice, bound,
          address,
          file._1,
          machine,
          config.timeout, config.nbRepetition,
          config.printOldAAM)
    }
  }


  /**
    * Run the machine on file with corresponding parameters.
    */
  def run(latticeValue: Config.Lattice.Value, bound: Int,
          addressValue: Config.Address.Value,
          file: Path,
          machineValue: Config.Machine.Value,
          timeout: Option[Duration], nbRepetition: Int,
          printOldAAM: Boolean = false) = {
    require(nbRepetition >= 0)

    val configConfig = new Config.Config

    val lattice: SchemeLattice = latticeValue match {
      case Config.Lattice.Concrete => new MakeSchemeLattice[Concrete.S, Concrete.B, Concrete.I, Concrete.F, Concrete.C, Concrete.Sym](configConfig.counting)

      case Config.Lattice.TypeSet =>
        new MakeSchemeLattice[Type.S, Concrete.B, Type.I, Type.F, Type.C, Type.Sym](configConfig.counting)

      case Config.Lattice.BoundedInt =>
        val bounded = new BoundedInteger(configConfig.bound)
        new MakeSchemeLattice[Type.S, Concrete.B, bounded.I, Type.F, Type.C, Type.Sym](configConfig.counting)

      case Config.Lattice.ConstantPropagation =>
        new MakeSchemeLattice[ConstantPropagation.S, Concrete.B, ConstantPropagation.I, ConstantPropagation.F, ConstantPropagation.C, ConstantPropagation.Sym](configConfig.counting)
    }
    implicit val isSchemeLattice: IsSchemeLattice[lattice.L] = lattice.isSchemeLattice

    val address: AddressWrapper = addressValue match {
      case Config.Address.Classical => ClassicalAddress
      case Config.Address.ValueSensitive => ValueSensitiveAddress
    }
    implicit val isAddress = address.isAddress

    val time: TimestampWrapper = ZeroCFA
    implicit val isTimestamp = time.isTimestamp

    val machine = machineValue match {
      case Config.Machine.AAM => new AAM[SchemeExp, lattice.L, address.A, time.T]
      case Config.Machine.AAMNS => new AAMNS[SchemeExp, lattice.L, address.A, time.T]
      case Config.Machine.AAMGlobalStore => new AAMAACP4F[SchemeExp, lattice.L, address.A, time.T](AAMKAlloc)
      case Config.Machine.ConcreteMachine => new ConcreteMachine[SchemeExp, lattice.L, address.A, time.T]
      case Config.Machine.AAC => new AAMAACP4F[SchemeExp, lattice.L, address.A, time.T](AACKAlloc)
      case Config.Machine.Free => new AAMAACP4F[SchemeExp, lattice.L, address.A, time.T](P4FKAlloc)
    }

    val sem = new SchemeSemantics[lattice.L, address.A, time.T](new SchemePrimitives[address.A, lattice.L])

    configConfig.file = Some(file.toString)
    configConfig.timeout = timeout

    val machineText: String =
      if (printOldAAM && (machineValue == Config.Machine.AAMNS)) "oldAAM"
      else machineValue.toString
    print(s"$latticeValue\t$bound\t$addressValue\t${file.toString}\tfalse\t$machineText\t1")
    System.out.flush

    val program: String = Util.fileContent(file.toString).getOrElse("")  // read Scheme program

    val finishedTimes: ArrayBuffer[Double] = ArrayBuffer.empty[Double]
    val errors: ArrayBuffer[String] = ArrayBuffer.empty[String]

    var firstRepetition: Boolean = true
    var anyTimedOut: Boolean = false
    var finalValuesString: String = ""
    var numberOfStates: Int = 0
    var anyError: Boolean = false

    for (_ <- 0 until nbRepetition) {
      System.gc  // ask to free memory
      Thread.sleep(1000)  // wait 1 second

      try {
        // Run the machine on the Scheme program
        val result = machine.eval(sem.parse(program), sem,
          configConfig.dotfile.nonEmpty, Timeout.start(timeout.map(_.toNanos)))

        // Print results
        val currentErrors: ArrayBuffer[String] = ArrayBuffer.empty[String]
        if (result.timedOut)
          currentErrors += "TimedOut"

        if (firstRepetition) {        // sets and prints reference values
          firstRepetition = false
          finalValuesString = result.finalValues.toList.sortWith(JoinLattice[lattice.L].totalLessThan).mkString(", ")
          numberOfStates = result.numberOfStates
          print(s"\t$numberOfStates\t\t\t\t${result.finalValues.size}\t$finalValuesString\t$nbRepetition")
        }
        else if (!result.timedOut) {  // compares to reference values
          val currentFinalValuesString: String = result.finalValues.mkString(", ")
          if (currentFinalValuesString != finalValuesString)
            currentErrors += s"Different final values! $currentFinalValuesString"
          if (result.numberOfStates != numberOfStates)
            currentErrors += s"Different # states! ${result.numberOfStates}"
        }
        print(s"\t${!result.timedOut}\t${result.time}")
        System.out.flush

        anyTimedOut |= result.timedOut
        if (!result.timedOut)
          finishedTimes += result.time

        if (currentErrors.isEmpty)
          errors += ""
        else {
          errors += currentErrors.mkString("; ")
          if (!result.timedOut)
            anyError = true
        }
      }
      catch {  // eval failed
        case e: Throwable =>
          errors += e.toString
          anyError = true
          // e.printStackTrace
          // System.exit(0)
      }
    }

    if (nbRepetition > 1) {
      if (finishedTimes.isEmpty)
        print("\t0\t\t")
      else {
        val nb: Int = finishedTimes.size
        val average: Double = finishedTimes.sum/nb
        val standardError: Double = finishedTimes.map(time => Math.abs(time - average)).sum/nb

        print(s"\t$nb\t$average\t$standardError")
      }
    }

    println(s"""\t${!anyError}\t${errors.mkString("\t")}""")
    System.out.flush
  }


  /**
    * Reads the file filename and returns set of files.
    *
    * The format of read filename is, for each line:
    * [#] FILENAME [# COMMENT]
    * Each line beginning by # is ignored.
    *
    * If filename doesn't exist then return an empty set.
    */
  def readExcepts(filename: String): Set[String] = {
    val excepts: ArrayBuffer[String] = ArrayBuffer.empty[String]

    if (new java.io.File(filename).exists) {
      val file = scala.io.Source.fromFile(filename)

      for (line <- file.getLines if !line.trim.startsWith("#")) {
        val except: String = line.split('#')(0).trim
        if (except != "")
          excepts += except
      }

      file.close
    }

    excepts.toSet
  }



  /** Parse command line and run */
  def main(args: Array[String]) {
    val parser = new scopt.OptionParser[ParamsConfig]("Scala-AM") {
      private val separator = ", "

      val now = java.util.Calendar.getInstance()
      head("Scala-AM",
        s"""$scalaAMVersion (executed with Java ${System.getProperty("java.version")} ${System.getProperty("java.vendor")})${if (Util.isAssertOn) " ASSERT ON!" else ""}""")

      help("help").text("Prints this usage text")

      version("version").text("Prints the version of Scala-AM and exits")

      note("")

      opt[String]("root")
        .action { (x, c) => c.copy(root = x) }
        .text(s"""Root path used to search Scheme programs ("${default.root}" by default)""")

      opt[String]('f', "files")
        .action { (x, c) => c.copy(filenamesPattern = s"glob:$x") }
        .text("Filename or glob pattern to describe file(s) contained Scheme programs to analyse (mutually exclusive with --regex-files option)")

      opt[String]("regex-files")
        .action { (x, c) => c.copy(filenamesPattern = s"regex:$x") }
        .text(s"""Regex pattern to describe file(s) contained Scheme programs to analyse ("${default.filenamesPattern.stripPrefix("regex:")}" by default)""")

      opt[String]("excepts-file")
        .action { (x, c) => c.copy(exceptsFilename = x) }
        .text("File contained a list of filename that will be exclude to --files or --regex-files options (disabled by default)")

      note("")

      opt[Seq[Config.Lattice.Value]]('l', "lattices")
        .action { (x, c) => c.copy(lattices = x) }
        .text(s"""List of lattices to use (${Config.Lattice.values.mkString(separator)}) (${default.lattices.mkString(",")} by default)""")

      opt[Unit]("all-lattices")
        .action { (x, c) => c.copy(lattices = Config.Lattice.values.toList) }
        .text("""Set all lattices for the --lattices option""")

      opt[Seq[Int]]('b', "bounds") action { (x, c) => c.copy(bounds = if (x.isEmpty) List(100) else x) } text(s"List of bounds for bounded lattice (defaults to ${default.bounds.mkString(",")})")

      opt[Seq[Config.Address.Value]]('a', "addresses") action { (x, c) => c.copy(addresses = x) } text(s"List of addresses to use (${Config.Address.values.mkString(separator)}) (${default.addresses.mkString(",")} by default)")

      opt[Seq[Config.Machine.Value]]('m', "machines")
        .action { (x, c) => c.copy(machines = x) }
        .text(s"""List of abstract machines to use (${Config.Machine.values.mkString(separator)}) (${default.machines.mkString(",")} by default)""")

      opt[Unit]("all-machines")
        .action { (x, c) => c.copy(machines = Config.Machine.values.toList) }
        .text("""Set all machines for the --machines option""")

      note("")

      opt[String]("prepare-file")
        .action { (x, c) => c.copy(filenamePrepare = x) }
        .text("""Scheme program use to "prepare" JVM before real benchmaks (disabled by default)""")

      opt[Int]('r', "repetition")
        .action { (x, c) => c.copy(nbRepetition = x) }
        .validate(x => if (x < 0) failure("Option --repetition must be > 0!")
                       else success)
        .text(s"Number of repetitions of each benchmark (${default.nbRepetition} by default)")

      opt[Duration]('t', "timeout")
        .action { (x, c) => c.copy(timeout = if (x.isFinite) Some(x) else None) }
        .validate(x => if (x.toNanos < 0) failure("Option --timeout must be > 0!")
                       else success)
        .text(s"Timeout in Duration format (${default.timeout} by default)")

      note("")

      opt[Unit]("no-header")
        .action { (_, c) => c.copy(headerEnabled = false) }
        .text("Disable header printing")

      opt[Unit]("print-oldAAM")
        .action { (_, c) => c.copy(printOldAAM = true) }
        .text("Print oldAAM as machine name instead AAMNS (this option is to use when this Scala-AM is calling by Scala-Par-AM)")

      note("""
All sources and more information about this version of Scala-AM are available on Bitbucket:
<https://bitbucket.org/OPiMedia/scala-am/>.
The original Scala-AM (written by Quentin Stiévenart) is available on GitHub:
<https://github.com/acieroid/scala-am>.""")
    }

    parser.parse(args, default) match  {
      case Some(config) =>  // Run all
        // println(config)

        if (config.filenamePrepare != "") {  // run on one Scheme program to "prepare" JVM
          for (lattice <- config.lattices;
               address <- config.addresses;
               machine <- config.machines)
            run(lattice, config.bounds(0),
                address,
                Paths.get(config.filenamePrepare),
                machine,
                config.timeout, 1,  // 1 repetition
                config.printOldAAM)
        }

        if (config.headerEnabled) {  // Print header
          def num(title: String): String =
            if (config.nbRepetition > 1)
              (1 to config.nbRepetition).map(i => s"$title $i").mkString
            else
              title

          // Some empty columns are added to have similar output than Scala-Par-AM
          print(s"""Lattice\tBound\tAddress\tScheme program\tStep filter?\tMachine\tp\t# states\t# error states\t# error values\t# final states\t# final values\tFinal values\t#${num("\t?\tTime")}""")

          if (config.nbRepetition > 1)
            print("\t# finished\tAverage time\tStandard error")

          println(s"""\tCorrect?${num("\tError")}""")
        }

        // Run all benchmarks
        foreachFile(config)

      case None => None     // Error in command line parameters
    }
  }
}
