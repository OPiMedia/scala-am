name := "scala-am"
version := "Fork"
organization := "be.opimedia"
homepage := Some(url("https://bitbucket.org/OPiMedia/scala-am"))
description := "Fork of Scala-AM - A Framework for Static Analysis of Dynamic Languages"


scalaVersion := "2.12.7"
scalacOptions ++= Seq("-J-Xmx3G", "-unchecked", "-deprecation", "-feature")
scalacOptions ++= Seq("-g:none", "-Xdisable-assertions", "-opt:unreachable-code", "-opt:simplify-jumps", "-opt:compact-locals", "-opt:copy-propagation", "-opt:redundant-casts", "-opt:box-unbox", "-opt:nullness-tracking", "-opt:l:inline", "-opt-inline-from")

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"  // parser: scala.util.parsing.combinator
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.2.0"
libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.2.28"  // functional programming
libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.1"  // command line options
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.23"  // concurrency for tests
libraryDependencies += "jline" % "jline" % "2.14.6"  // input code from command line
libraryDependencies += "org.json4s" %% "json4s-native" % "3.6.7"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.7"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"

maxErrors := 5
mainClass in (Compile, run) := Some("Main")


// Build complete JAR file with project (without test) and all libraries included Scala
test in assembly := {}
mainClass in assembly := Some("Main")
assemblyJarName in assembly := "../../scala-am.jar"
