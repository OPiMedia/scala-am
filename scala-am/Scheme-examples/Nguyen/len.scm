;;; Compute the length of a list
(define (len l) (loop l (lambda (x) x)))
(define (loop l k)
  ;;; MODIFIED
  ;;; (cond [(empty? l) (k 0)]
  ;;;       [(cons? l)
  ;;;        (loop (rest l) (lambda (n) (k (+ 1 n))))]))
  (cond ((null? l) (k 0))
        ((pair? l)
         (loop (cdr l) (lambda (n) (k (+ 1 n)))))))


;;; ADDED to run something
(len (list 1 2 3 4 5))
