;;; Ackermann function
(define (ack m n)
  (cond ((= 0 m) (+ 1 n))
        ((= 0 n) (ack (- m 1) 1))
        (else
         (ack (- m 1)
              (ack m (- n 1))))))

(ack 2 0)
