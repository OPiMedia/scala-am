;;; ADAPTED to avoid Scala-(Par-)AM limitations

(define (create-graph eq-label-ft directed? weighted?)
  (let ((nodes '()))
    ;edge abstraction
    ;;; MODIFIED (define (make-edge node1 node2 . info)
    (define (make-edge node1 node2 info)
      (if weighted?
          (list node1 node2 '() (car info))
          (list node1 node2 '() 'notused)))
    (define (get-first-node edge)
      (car edge))
    (define (get-second-node edge)
      (cadr edge))
    (define (get-next-edge edge)
      (caddr edge))
    (define (set-next-edge! edge next-edge)
      (set-car! (cddr edge) next-edge))
    (define (get-edge-info edge)
      (cadddr edge))
    (define (set-edge-info! edge info)
      (set-car! (cdddr edge) info))
    ;node abstraction
    ;;; MODIFIED (define (make-node label info . status)
    (define (make-node label info status)
      (list label info '() (if (null? status) 'not-used (car status)))) ;*
    (define (get-label node)
      (car node))
    (define (set-label! node label)
      (set-car! node label))
    (define (get-node-info node)
      (cadr node))
    (define (set-node-info! node a-info)
      (set-car! (cdr node) a-info))
    (define (get-node-status node)
      (cadddr node))
    (define (set-node-status! node status)
      (set-car! (cdddr node) status))
    (define (get-edges node)
      (caddr node))
    (define (set-edges! node edges)
      (set-car! (cddr node) edges))
    (define (add-to-edges node edge)
      (set-next-edge! edge (get-edges node))
      (set-edges! node edge))
    ;some help
    (define (find-node label)
      (define (find-iter current)
        (cond
          ((null? current) #f)
          ((eq-label-ft label (get-label (car current))) (car current))
          (else (find-iter (cdr current)))))
      (find-iter nodes))
    (define (find-edge label1 label2)
      (define (find-iter current node)
        (cond
          ((null? current) #f)
          ((eq? node (get-second-node current)) current)
          (else (find-iter (get-next-edge current) node))))
      (let ((node1 (find-node label1))
            (node2 (find-node label2)))
        (if directed?
            (if (and node1 node2) ;*
                (find-iter (get-edges node1) node2)
                #f) ;*
            (if (and node1 node2) ;*
                (cons (find-iter (get-edges node1) node2)
                      (find-iter (get-edges node2) node1))
                (cons #f #f))))) ;*
    ;the real stuff
    (define (insert-node label info)
      (let ((node (find-node label)))
        (cond
          (node (set-node-info! node info))
          ;;; MODIFIED (else (set! nodes (cons (make-node label info) nodes))
          (else (set! nodes (cons (make-node label info '()) nodes))
                #t))))
    ;;; MODIFIED (define (insert-edge label1 label2 . info)
    (define (insert-edge label1 label2 info)
      (let ((edge (find-edge label1 label2)))
        (cond
          ((and directed? edge)
           (if weighted? (set-edge-info! edge (car info)))
           #t)
          ((and (not directed?) (car edge) (cdr edge))
           (if weighted?
               (begin (set-edge-info! (car edge) (car info))
                      (set-edge-info! (cdr edge) (car info))))
           #t)
          (else
            (let* ((node1 (find-node label1))
                   (node2 (find-node label2)))
              (if (and node1 node2)
                  (let ((edge (if weighted?
                                  ;;; MODIFIED
                                  ;;; (make-edge node1 node2 (car info))
                                  ;;; (make-edge node1 node2))))
                                  (make-edge node1 node2 (list (car info)))
                                  (make-edge node1 node2 '()))))
                    (if directed?
                        (add-to-edges node1 edge)
                        (let ((edge-dupl (if weighted?
                                             ;;; MODIFIED
                                             ;;; (make-edge node2 node1 (car info))
                                             ;;; (make-edge node2 node1))))
                                             (make-edge node2 node1 (list (car info)))
                                             (make-edge node2 node1 '()))))
                          (add-to-edges node1 edge)
                          (add-to-edges node2 edge-dupl)))
                    #t)
                  #f))))))
    (define (delete-edge label1 label2)
      (define (delete-iter current previous node1 node2)
        (cond
          ((null? current) #f)
          ((eq? (get-second-node current) node2)
           (if previous
               (set-next-edge! previous (get-next-edge current))
               (set-edges! node1 (get-next-edge current)))
           #t)
          (else
            (delete-iter (get-next-edge current) current node1 node2))))
      (let ((node1 (find-node label1))
            (node2 (find-node label2)))
        (if (and node1 node2)
            (if directed?
                (delete-iter (get-edges node1) #f node1 node2)
                (and
                  (delete-iter (get-edges node1) #f node1 node2)
                  (delete-iter (get-edges node2) #f node2 node1)))
            #f)))
    (define (delete-node label)
      ;;;assumes that all-edges involving this node have been removed
      ;;;try to implement implicit removal of all edges as an exercise
      (define (delete-iter current prev)
        (cond
          ((null? current) #f)
          ((eq-label-ft label (get-label (car current)))
           (if prev
               (set-cdr! prev (cdr current))
               (set! nodes (cdr nodes)))
           #t)
          (else (delete-iter (cdr current) current))))
      (delete-iter nodes #f))
    (define (lookup-node-info label)
      (let ((node (find-node label)))
        (if node
            (get-node-info node)
            #f)))
    (define (change-node-info label info)
      (let ((node (find-node label)))
        (if node
            (set-node-info! node info)
            #f)))
    (define (lookup-node-status label)
      (let ((node (find-node label)))
        (if node
            (get-node-status node)
            #f)))
    (define (change-node-status label status)
      (let ((node (find-node label)))
        (if node
            (set-node-status! node status)
            #f)))
    (define (lookup-edge label1 label2)
      (let ((edge (find-edge label1 label2)))
        (if directed?
            (if edge
                (if weighted? (get-edge-info edge) #t)
                #f)
            (if (and (car edge) (cdr edge))
                (if weighted? (get-edge-info (car edge)) #t)
                #f))))
    (define (empty?)
      (null? nodes))
    (define (map-over-nodes a-function)
      ;a-function takes two argument, i.e. label and info of the node
      (define (map-iter current result)
        (if (null? current)
             (reverse result)
            (map-iter (cdr current)
                      (cons (a-function (get-label (car current))
                                        (get-node-info (car current)))
                            result))))
      (map-iter nodes '()))
    (define (foreach-node a-action)
      ;a-action takes two argument, i.e. label and info of the node
      (define (foreach-iter current)
        (cond
          ((null? current) #t)
          (else
            (a-action (get-label (car current)) (get-node-info (car current)))
            (foreach-iter (cdr current)))))
      (foreach-iter nodes))
    (define (map-over-neighbours label a-function)
      ;;; a-function takes 5 arguments
      ;;; from-label from-info to-label to-info edge-info
      ;;; edge-info is not used in a non weighted graph
      (let ((node (find-node label)))
        (define (edges-iter current result)
          (if (null? current)
              result
              (let* ((neigbour-node (get-second-node current))
                     (res (a-function
                            (get-label node)
                            (get-node-info node)
                            (get-label neigbour-node)
                            (get-node-info neigbour-node)
                            (get-edge-info current))))
                (edges-iter (get-next-edge current) (cons res result)))))
        (if node
            (edges-iter (get-edges node) '())
            #f)))
    (define (foreach-neighbour label a-action)
      ;;; a-action takes 5 arguments
      ;;; from-label from-info to-label to-info edge-info
      ;;; edge-info is not used in a non weighted graph
      (let ((node (find-node label)))
        (define (edges-iter current)
          (if (null? current)
              #t
              (let* ((neigbour-node (get-second-node current)))
                     (a-action
                       (get-label node)
                       (get-node-info node)
                       (get-label neigbour-node)
                       (get-node-info neigbour-node)
                       (get-edge-info current))
                (edges-iter (get-next-edge current)))))
        (if node
            (edges-iter (get-edges node))
            #f)))
    ;note that there are more messages supported than these in the
    ;spec of the graph ADT, these extra messages come in handy when
    ;the graph traversals have to be implemented
    ;;; MODIFIED (define (dispatch msg . args) ;*
    (define (dispatch msg args) ;*
      (cond
        ((eq? msg 'empty?) (empty?))
        ((eq? msg 'insert-node) (apply insert-node args))
        ((eq? msg 'delete-node) (apply delete-node args))
        ;;; MODIFIED ((eq? msg 'insert-edge) (apply insert-edge args))
        ((eq? msg 'insert-edge) (apply insert-edge (car args) (cadr args) (cddr args)))
        ((eq? msg 'delete-edge) (apply delete-edge args))
        ((eq? msg 'lookup-node-info) (apply lookup-node-info args))
        ((eq? msg 'change-node-info) (apply change-node-info args))
        ((eq? msg 'lookup-node-status) (apply lookup-node-status args))
        ((eq? msg 'change-node-status) (apply change-node-status args))
        ((eq? msg 'lookup-edge) (apply lookup-edge args))
        ((eq? msg 'map-over-nodes) (apply map-over-nodes args))
        ((eq? msg 'foreach-node) (apply foreach-node args))
        ((eq? msg 'map-over-neighbours) (apply map-over-neighbours args))
        ((eq? msg 'foreach-neighbour) (apply foreach-neighbour args))
        ((eq? msg 'find-node) (apply find-node args))
        (else
          (error "unknown request -- create-graph" msg))))
    dispatch))


;;; ADDED and adapted from DFS/graf-test.scm to run something
(define g (create-graph eq? #t #f))
(g 'insert-node (list 'z '()))
(g 'insert-node (list 'y '()))
(g 'insert-node (list 'x '()))
(g 'insert-node (list 'w '()))
(g 'insert-node (list 'v '()))
(g 'insert-node (list 'u '()))

(g 'insert-edge (list 'u 'x '()))
(g 'insert-edge (list 'u 'v '()))
(g 'insert-edge (list 'v 'y '()))
(g 'insert-edge (list 'x 'v '()))
(g 'insert-edge (list 'y 'x '()))
(g 'insert-edge (list 'w 'y '()))
(g 'insert-edge (list 'w 'z '()))
(g 'insert-edge (list 'z 'z '()))


(g 'foreach-node (list (lambda (l i)
                         (display l)
                         (display " << ")
                         (display i)
                         (display " >> ")
                         (newline))))
(g 'foreach-node (list (lambda (l i)
                         (display l)
                         (display " << ")
                         (display i)
                         (display " >> ")
                         (newline))))
