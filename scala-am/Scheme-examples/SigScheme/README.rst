.. -*- restructuredtext -*-

===========================
Scheme examples / SigScheme
===========================
Examples taken (2018-02-28) from
https://github.com/uim/sigscheme/tree/master/bench

Added/adapted tests to some files.
* takr.scm should return 7.
* loop.scm should evaluate to 8000.
* let-loop.scm should return 20000.
* arithint.scm should evaluate to 20001.

Some similar benchmarks were already included in SCALA-AM's test suite and are not included again.
* bench-fib.scm    (identical to Larceny/Numerical/fib.scm)
* bench-cpstak.scm (identical to Larceny/Gabriel/cpstak.scm)
* bench-tak.scm    (identical to Larceny/Numerical/tak.scm)
* bench-takl.scm   (identical to Larceny/Gabriel/takl.scm)
