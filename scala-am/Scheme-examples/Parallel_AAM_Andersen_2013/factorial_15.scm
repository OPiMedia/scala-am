;; Factorial of 15
;; Example from paper "Multi-core Parallelization of Abstracted Abstract Machines" (Leif Andersen and Matthew Might, 2013).
(letrec ((f (lambda (n)
              (if (= n 0)
                  1
                  (* n (f (- n 1)))))))
  (f 15))
