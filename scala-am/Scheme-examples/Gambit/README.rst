.. -*- restructuredtext -*-

========================
Scheme examples / Gambit
========================
Scheme benchmarks of Gambit
https://github.com/gambit/gambit/tree/master/bench/src

These benchmarks are contained in Larceny benchmarks:
* boyer.scm   (identical to Larceny/Gabriel/boyer.scm)
* browse.scm  (identical to Larceny/Gabriel/browse.scm)
* cpstak.scm  (identical to Larceny/Gabriel/cpstak.scm)
* ctak.scm    (identical to Larceny/Gabriel/ctak.scm)
* dderiv.scm  (identical to Larceny/Gabriel/dderiv.scm)
* deriv.scm   (identical to Larceny/Gabriel/deriv.scm)
* destruc.scm (identical to Larceny/Gabriel/destruc.scm)
* diviter.scm (identical to Larceny/Gabriel/diviter.scm)
* divrec.scm  (identical to Larceny/Gabriel/divrec.scm)
* puzzle.scm  (identical to Larceny/Gabriel/puzzle.scm)
* takl.scm    (identical to Larceny/Gabriel/takl.scm)
* triangl.scm (identical to Larceny/Gabriel/triangl.scm)

* ack.scm    (identical to Larceny/Kernighan_and_Van_Wyk/ack.scm)
* array1.scm (identical to Larceny/Kernighan_and_Van_Wyk/array1.scm)
* cat.scm    (identical to Larceny/Kernighan_and_Van_Wyk/cat.scm)
* tail.scm   (identical to Larceny/Kernighan_and_Van_Wyk/tail.scm)
* wc.scm     (identical to Larceny/Kernighan_and_Van_Wyk/wc.scm)

* string.scm  (identical to Larceny/Kernighan_and_Van_Wyk/string.scm)
* sumloop.scm (identical to Larceny/Kernighan_and_Van_Wyk/sumloop.scm)

* fib.scm (identical to Larceny/Numerical/fib.scm)
* sum.scm (identical to Larceny/Numerical/sum.scm)
* tak.scm (identical to Larceny/Numerical/tak.scm)

* conform.scm   (identical to Larceny/other/conform.scm)
* dynamic.scm   (identical to Larceny/other/dynamic.scm)
* earley.scm    (identical to Larceny/other/earley.scm)
* fibc.scm      (identical to Larceny/other/fibc.scm)
* gcbench.scm   (identical to Larceny/other/gcbench.scm)
* graphs.scm    (identical to Larceny/other/graphs.scm)
* lattice.scm   (identical to Larceny/other/lattice.scm)
* matrix.scm    (identical to Larceny/other/matrix.scm)
* mazefun.scm   (identical to Larceny/other/mazefun.scm)
* maze.scm      (identical to Larceny/other/maze.scm)
* nboyer.scm    (identical to Larceny/other/nboyer.scm)
* nqueens.scm   (identical to Larceny/other/nqueens.scm)
* paraffins.scm (identical to Larceny/other/paraffins.scm)
* perm9.scm     (identical to Larceny/other/perm9.scm)
* peval.scm     (identical to Larceny/other/peval.scm)
* primes.scm    (identical to Larceny/other/primes.scm)
* sboyer.scm    (identical to Larceny/other/sboyer.scm)


These following examples are not included (because use of non-standard primitives or Scala-(Par-)AM limitations):
* chud1K.scm
* chud100K.scm
* compiler.scm
* crash.scm
* fail.scm
* fft.scm
* fftrad4.scm
* fibpf.scm
* mbrot.scm
* nucleic.scm
* pi.scm
* pi10K.scm
* pnpoly.scm
* ray.scm
* scheme.scm
* simplex.scm
* slatex.scm
* succeed.scm
* sum1.scm
* sumfp.scm
* test.scm
* tfib.scm
* trav1.scm
* trav2.scm
