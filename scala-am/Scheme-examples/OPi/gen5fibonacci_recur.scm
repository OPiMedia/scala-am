;; nth number of a similar Fibonacci sequence (by recursive process)
(define (gen5fibonacci n)
  (if (<= n 4)
      n
      (+ (gen5fibonacci (- n 1))
         (gen5fibonacci (- n 2))
         (gen5fibonacci (- n 3))
         (gen5fibonacci (- n 4))
         (gen5fibonacci (- n 5)))))

(gen5fibonacci 10)  ;; 294
