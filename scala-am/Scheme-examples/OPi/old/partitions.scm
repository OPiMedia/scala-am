;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; (DS math partitions)              ;;;
;;; (c) Olivier Pirson --- DragonSoft ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-module (DS math partitions))
(define-public DS-math-partitions-VERSION "2006 June 7")

;;; On appelle composition d'un naturel n
;;;   une suite de naturels > 0 dont la somme vaut n

;;; On appelle partition d'un naturel n
;;;   une composition de n, abstraction faite de l'ordre des termes
;;;
;;; Exemples : Partition  de 0 : partition vide
;;;            Partition  de 1 : 1
;;;            Partitions de 2 : 2
;;;                              1 + 1
;;;            Partitions de 3 : 3
;;;                              2 + 1
;;;                              1 + 1 + 1
;;;            Partitions de 4 : 4
;;;                              3 + 1
;;;                              2 + 2
;;;                              2 + 1 + 1
;;;                              1 + 1 + 1 + 1
;;;            Partitions de 5 : 5
;;;                              4 + 1
;;;                              3 + 2
;;;                              3 + 1 + 1
;;;                              2 + 2 + 1
;;;                              2 + 1 + 1 + 1
;;;                              1 + 1 + 1 + 1 + 1

;;; Pour tout naturel n :
;;;   C(n)   := nombre de compositions de n = 2^(n-1)
;;;   P(n)   := nombre de partitions de n
;;;   P_k(n) := nombre de partitions de n qui contient exactement k termes
;;;
;;;   n |  C(n) | P(n) | P_0(n) | P_1(n) | P_2(n) | P_3(n) | P_4(n) | P_5(n) | P_6(n) | ... | P_n(n) | P_(n+1)(n)
;;; --------------------------------------------------------------------------------------------------------------
;;;   0 |     1 |    1 |      1 |        |        |        |        |        |        |     |      1 |          0
;;;   1 |     1 |    1 |      0 |      1 |        |        |        |        |        |     |      1 |          0
;;;   2 |     2 |    2 |      0 |      1 |      1 |        |        |        |        |     |      1 |          0
;;;   3 |     4 |    3 |      0 |      1 |      1 |      1 |        |        |        |     |      1 |          0
;;;   4 |     8 |    5 |      0 |      1 |      2 |      1 |      1 |        |        |     |      1 |          0
;;;   5 |    16 |    7 |      0 |      1 |      2 |      2 |      1 |      1 |        |     |      1 |          0
;;;   6 |    32 |   11 |      0 |      1 |      3 |      3 |      2 |      1 |      1 |     |      1 |          0
;;;   7 |    64 |   15 |      0 |      1 |      3 |      4 |      3 |      2 |      1 |     |      1 |          0
;;;   8 |   128 |   22 |      0 |      1 |      4 |      5 |      5 |      3 |      2 |     |      1 |          0
;;;   9 |   256 |   30 |      0 |      1 |      4 |      7 |      6 |      5 |      3 |     |      1 |          0
;;;  10 |   512 |   42 |      0 |      1 |      5 |      8 |      9 |      7 |      5 |     |      1 |          0
;;; ... |       |      |        |        |        |        |        |        |        |     |        |
;;;   n |2^(n-1)|      |      0 |      1 |        |        |        |        |        |     |      1 |          0



;;;;;;;;;;;;;;;;;
;;; Fonctions ;;;
;;;;;;;;;;;;;;;;;
;;; Renvoie une liste contenant toutes les partitions de n ("ordre decroissant")
;;; pre: n exact natural
;;; result: liste de liste d'exact natural > 0
;;;         ou si n = 0, liste ayant pour seul element '()
;;; O(...)
(define-public (partitions n)
  (if (zero? n)
      (list '())
      (let ((p (list n 0))  ; partition en construction (ordre inverse) se terminant par 0
            (l '()))        ; liste des partitions (dans l'ordre)
        (set! n 0)  ; ce qu'il manque a la somme des termes de p
        (while (not (negative? (car p)))
               ;; inv: ce qu'il manque a la somme des termes de p
               (while (> n (car p))
                      ;; Il manque des termes pour p et le dernier convient
                      (set! n (- n (car p)))
                      (set! p (cons (car p) p)))
               (if (positive? n)
                   (begin  ;; Il manque le terme n pour p
                     (set! p (cons n p))
                     (set! n 0)))
               ;; p forme une partition
               (set! l (append! l (list (cdr (reverse p)))))
               (while (= 1 (car p))
                      ;; Remonte dans les termes de p jusqu'a trouver un terme > 1
                      (set! p (cdr p))
                      (set! n (1+ n)))
               ;; Decremente le dernier terme de p
               (set! n (1+ n))
               (set-car! p (1- (car p))))
        l)))

;;; Renvoie C(n), le nombre de compositions de n
;;; pre: n exact natural
;;; result: exact natural
;;; O(...)
(define-public (partitions-C n)
  (if (zero? n)
      1
      (integer-expt 2 (1- n))))

;;; Renvoie P(n), le nombre de partitions de n
;;; pre: n exact natural
;;; result: exact natural
;;; O(...)
(define-public (partitions-P n)
  (if (zero? n)
      1
      (let ((p (list n 0))  ; partition en construction (ordre inverse) se terminant par 0
            (nb 0))         ; nombre de partitions
        (set! n 0)  ; ce qu'il manque a la somme des termes de p
        (while (not (negative? (car p)))
               ;; inv: ce qu'il manque a la somme des termes de p
               (while (> n (car p))
                      ;; Il manque des termes pour p et le dernier convient
                      (set! n (- n (car p)))
                      (set! p (cons (car p) p)))
               (if (positive? n)
                   (begin  ;; Il manque le terme n pour p
                     (set! p (cons n p))
                     (set! n 0)))
               ;; p forme une partition
               (set! nb (1+ nb))
               (while (= 1 (car p))
                      ;; Remonte dans les termes de p jusqu'a trouver un terme > 1
                      (set! p (cdr p))
                      (set! n (1+ n)))
               ;; Decremente le dernier terme de p
               (set! n (1+ n))
               (set-car! p (1- (car p))))
        nb)))

;;; Renvoie P_k(n), le nombre de partitions de n qui contient exactement k termes
;;; pre: n exact natural
;;;      k exact natural
;;; result: exact natural
;;; O(...)
(define-public (partitions-Pk n k)
  (if (zero? n)
      (if (zero? k)
          1
          0)
      (let ((p (list n 0))  ; partition en construction (ordre inverse) se terminant par 0
            (len 1)         ; nombre de termes de p
            (nb 0))         ; nombre de partitions
        (set! n 0)  ; ce qu'il manque a la somme des termes de p
        (while (not (negative? (car p)))
               ;; inv: ce qu'il manque a la somme des termes de p
               (while (> n (car p))
                      ;; Il manque des termes pour p et le dernier convient
                      (set! n (- n (car p)))
                      (set! p (cons (car p) p))
                      (set! len (1+ len)))
               (if (positive? n)
                   (begin  ;; Il manque le terme n pour p
                     (set! p (cons n p))
                     (set! n 0)
                     (set! len (1+ len))))
               ;; p forme une partition
               (if (= len k)
                   (set! nb (1+ nb)))
               (while (= 1 (car p))
                      ;; Remonte dans les termes de p jusqu'a trouver un terme > 1
                      (set! p (cdr p))
                      (set! n (1+ n))
                      (set! len (1- len)))
               ;; Decremente le dernier terme de p
               (set! n (1+ n))
               (set-car! p (1- (car p))))
        nb)))

;;; Renvoie P(n | termes pairs),
;;;   le nombre de partitions de n pour lesquelles tous les termes sont pairs
;;; pre: n exact natural
;;; result: exact natural
;;; O(...)
(define-public (partitions-P-even n)
  (if (or (odd? n) (zero? n))
      (if (zero? n)
          1
          0)
      (let ((p (list n 0))  ; partition en construction (ordre inverse) se terminant par 0
            (nb 0))         ; nombre de partitions
        (set! n 0)
        (while (not (negative? (car p)))
               ;; inv: ce qu'il manque a la somme des termes de p
               (while (> n (car p))
                      ;; Il manque des termes pour p et le dernier convient
                      (set! n (- n (car p)))
                      (set! p (cons (car p) p)))
               (if (positive? n)
                   (begin ;; Il manque le terme n pour p
                     (set! p (cons n p))
                     (set! n 0)))
               ;; p forme une partition
               (set! nb (1+ nb))
               (while (= 2 (car p))
                      ;; Remonte dans les termes de p jusqu'a trouver un terme > 2
                      (set! p (cdr p))
                      (set! n (+ n 2)))
               ;; Decremente le dernier terme de p
               (set! n (+ n 2))
               (set-car! p (- (car p) 2)))
        nb)))

;;; Renvoie P(n | termes impairs),
;;;   le nombre de partitions de n pour lesquelles tous les termes sont impairs
;;;   = P(n | termes distincts)
;;; pre: n exact natural
;;; result: exact natural
;;; O(...)
(define-public (partitions-P-odd n)
  (if (zero? n)
      1
      (let ((p (list (if (odd? n)  ; partition en construction (ordre inverse) se terminant par 0
                         n
                         (1- n))
                     0))
            (nb 0))                ; nombre de partitions
        (set! n (if (odd? n)
                    0
                    1))
        (while (not (negative? (car p)))
               ;; inv: ce qu'il manque a la somme des termes de p
               (while (> n (car p))
                      ;; Il manque des termes pour p et le dernier convient
                      (set! n (- n (car p)))
                      (set! p (cons (car p) p)))
               (if (odd? n)
                   ;; Il manque le terme n pour p
                   (set! p (cons n p))
                   (if (positive? n)
                       ;; Il manque les termes (n-1) et 1 pour p
                       (set! p (cons 1 (cons (1- n) p)))))
               (set! n 0)
               ;; p forme une partition
               (set! nb (1+ nb))
               (while (= 1 (car p))
                      ;; Remonte dans les termes de p jusqu'a trouver un terme > 1
                      (set! p (cdr p))
                      (set! n (1+ n)))
               ;; Decremente le dernier terme de p
               (set! n (+ n 2))
               (set-car! p (- (car p) 2)))
        nb)))


;;; Main
(partitions 11)
(partitions-C 11)
(partitions-P 11)
(partitions-Pk 11 3)
(partitions-P-even 11)
(partitions-P-odd 11)
