;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Fibonacci                         ;;;
;;;   Affiche les termes consecutifs  ;;;
;;;   de la suite de Fibonacci.       ;;;
;;; (c) Olivier Pirson --- DragonSoft ;;;
;;; Debute le 24 fevrier 2004         ;;;
;;; v.01.00.00 --- 14 avril 2004      ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define VERSION "v.01.00.00 --- 2004 April 14")
(define DRAGONSOFT "(c) Olivier Pirson --- DragonSoft")

;;; Suite de Fibonacci :
;;;   F(0)   := 0
;;;   F(1)   := 1
;;;   F(n+2) := F(n+1) + F(n), pour tout n naturel
;;;
;;; 0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765 10946 17711 28657...
;;;
;;; ==> pour tout q naturel :
;;;     F(2q+1) = F(q+1)^2 + F(q)^2
;;;     F(2q+2) = F(q+2)^2 - F(q)^2
;;;     F(2q+3) = F(q+2)^2 + F(q+1)^2

;;; Renvoie la paire ( F(n) . F(n+1) )
;;; PRE: n : naturel
(define (Fibonacci2 n)  ; O(log(n))
  (if (>= n 2)
      (let* ((FqFq1 (Fibonacci2 (quotient (- n 1) 2)))  ; ( F(q) . F(q+1) ) ou q = (n-1)/2
             (Fq  (car FqFq1))  ; F(q  )
             (Fq1 (cdr FqFq1))  ; F(q+1)
             (Fq2 (+ Fq1 Fq)))  ; F(q+2) = F(q+1) + F(q)
        (set! Fq  (* Fq  Fq ))  ; F(q  )^2
        (set! Fq1 (* Fq1 Fq1))  ; F(q+1)^2
        (set! Fq2 (* Fq2 Fq2))  ; F(q+2)^2
        (if (odd? n)
            (cons (+ Fq1 Fq) (- Fq2 Fq))  ; F(n  ) = F(2q+1) = F(q+1)^2 + F(q)^2
                                          ; F(n+1) = F(2q+2) = F(q+2)^2 - F(q)^2
            (cons (- Fq2 Fq) (+ Fq2 Fq1))))  ; F(n  ) = F(2q+2) = F(q+2)^2 - F(q  )^2
                                             ; F(n+1) = F(2q+3) = F(q+2)^2 + F(q+1)^2
      (cons n 1)))  ;    (F(0)=0 . F(1)=1)
                    ; ou (F(1)=1 . F(2)=1)

;;; Affiche F(from) F(from+1) ... F(to)
;;; PRE: from, to : naturels
(define (FibonacciFromTo from to)  ; O( max(Fibonacci2(from), to-from+1) )
  (let* ((FiFi1 (Fibonacci2 from))  ; ( F(from) . F(from+1) )
         (Fi  (car FiFi1))   ; F(from  )
         (Fi1 (cdr FiFi1)))  ; F(from+1)
    (do ((i from (+ i 2)))  ; O(to-from+1)
        ((> i to))
      (display Fi)
      (display " ")
      (if (< i to)
          (begin
            (display Fi1)
            (display " ")))
      (set! Fi  (+ Fi Fi1))    ; F(i)   = F(i-2) + F(i-1)
      (set! Fi1 (+ Fi Fi1))))  ; F(i+1) = F(i-1) + F(i)
  (newline))

;;; Affiche le message d'aide sur la sortie des erreurs.
(define (help)
  (display "Fibonacci from [to]\n")
  (display (string-append "  " DRAGONSOFT "\n"))
  (display (string-append "          " VERSION "\n"))
  (display "  Print F(from) F(from+1) ... F(to)\n")
  (display "  with F(0)   := 0\n")
  (display "       F(1)   := 1\n")
  (display "       F(n+2) := F(n+1) + F(n), n positive integer\n")
  (exit 1))

;;;;;;;;;;;;
;;; main ;;;
;;;;;;;;;;;;
(FibonacciFromTo 10 100)

(let ((nb (length (command-line))))
  (if (or (< nb 2) (> nb 3))
      (help))
  (let* ((from (car(cdr(command-line))))
         (to (car(cdr(cdr(append (command-line) (list from)))))))
    (set! from (string->number from))
    (set! to (string->number to))
    (if (< from 0)
        (help))
    (FibonacciFromTo from to)))
