;; nth number of a similar Fibonacci sequence (by recursive process)
(define (gen3fibonacci n)
  (if (<= n 2)
      n
      (+ (gen3fibonacci (- n 1))
         (gen3fibonacci (- n 2))
         (gen3fibonacci (- n 3)))))

(gen3fibonacci 10)  ;; 230
