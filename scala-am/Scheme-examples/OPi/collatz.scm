;;; Collatz function <https://oeis.org/A014682>
(define (collatz n)
  (if (even? n)
      (quotient n 2)
      (quotient (+ (* n 3) 1) 2)))

(collatz 7)  ;; 11
