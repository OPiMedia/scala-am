;; nth number of Fibonacci (by iterative process)
(define (fibonacci n)
  (let ((fn_1 1)
        (fn 0))
    (define (loop n)
      (if (> n 0)
          (let ((tmp fn))
            (set! fn (+ fn fn_1))
            (set! fn_1 tmp)
            (loop (- n 1)))))
    (loop n)
    fn))

(fibonacci 10)  ;; 55
