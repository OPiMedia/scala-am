;; nth number of Fibonacci (by iterative process)
(define (fibonacci n)
  (define (fibonacci2 n fn_1 fn)
    (if (= n 0)
        (cons fn_1 fn)
        (fibonacci2 (- n 1) fn (+ fn fn_1))))

  (cdr (fibonacci2 n 1 0)))

(fibonacci 10)  ;; 55
