#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
linear.py (March 13, 2019)
"""

import sys


def main():
    nb = (int(sys.argv[1]) if len(sys.argv) > 1
          else 1)

    print(';;; {} begin'.format(nb))

    for i in range(1, nb + 1):
        print('(begin {})'.format(i))

if __name__ == '__main__':
    main()
