.. -*- restructuredtext -*-

========================
Scheme examples / Sergey
========================
Examples taken (2019-03-01) from
https://github.com/ilyasergey/reachability/tree/master/benchmarks

These Scheme programs have been used in this paper:
J. Ian Johnson, Ilya Sergey, Christopher Earl, Matthew Might, and David Van Horn.
"Pushdown flow analysis with abstract garbage collection",
Journal of Functional Programming 24.2 (May 2014)

* https://www.cambridge.org/core/journals/journal-of-functional-programming/article/pushdown-flow-analysis-with-abstract-garbage-collection/92567231A16859F51056AB6F6781ACAE
* http://matt.might.net/papers/johnson2014ipdcfa.pdf

kcfa/rsa.scm is identical that jfp/rsa.scm,
except some display commands at the end.

These following examples are not included (because use of Scala-(Par-)AM limitations):
* examples/meta-circ.scm
* examples/scheme2c.scm
* jfp/scm2java.scm
* kcfa/higher-order-confusion.scm (incorrect program)
* kcfa/meta-circ.scm
* kcfa/scheme2java.scm
* kcfa/scheme-to-c.scm
