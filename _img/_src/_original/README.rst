.. -*- restructuredtext -*-

===============
Original images
===============

* cogwheels-306402.svg –
  https://pixabay.com/fr/vectors/roues-dent%C3%A9es-engrenages-grilles-306402/
* iconfinder_multimedia-23_809540.svg (Zlatko Najdenovski) –
  https://www.iconfinder.com/icons/809540/monitor_multimedia_music_screen_technology_icon
* *Le Calcul Mental* (Magritte, 1940)
  http://www.maths-et-tiques.fr/images/M_images/magritteCM.jpg
